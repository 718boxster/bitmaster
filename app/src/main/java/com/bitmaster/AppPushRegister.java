package com.bitmaster;

import android.app.Application;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONObject;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class AppPushRegister extends Application {
    TinyDB tinyDB;
    @Override
    public void onCreate() {
        super.onCreate();
        tinyDB = new TinyDB(this);
        String user_no = TextUtils.isEmpty(tinyDB.getString("user_no")) ? "0" : tinyDB.getString("user_no");
        String token;
//        if(TextUtils.isEmpty(tinyDB.getString("token"))){
//            tinyDB.putString("token", FirebaseInstanceId.getInstance().getToken());
//        }
//        token = tinyDB.getString("token");
//
//        PushRegister pushRegister = new PushRegister();
//        pushRegister.execute(user_no, token);
    }

    class PushRegister extends AsyncTask<String, String ,String>{

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            FormBody.Builder formBodyBuilder = new FormBody.Builder();
            formBodyBuilder.add("no", strings[0]);
            formBodyBuilder.add("token", strings[1]);
            FormBody formBody = formBodyBuilder.build();
            try{
                Request request = new Request.Builder().url("http://13.125.241.78:3000/user/add_token").post(formBody).build();
                Response response = client.newCall(request).execute();
                String myResponse = response.body().string();
                return myResponse;
            } catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.d("push result", s);
            try{
                JSONArray resArray = new JSONArray(s);
                JSONObject resObj = resArray.getJSONObject(0);
                String user_no = resObj.getString("u_no");
                tinyDB.putString("user_no", user_no);

                Log.d("registered", tinyDB.getString("user_no") + " / " + tinyDB.getString("token"));
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
