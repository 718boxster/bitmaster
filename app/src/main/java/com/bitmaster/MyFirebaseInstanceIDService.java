package com.bitmaster;

import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import org.json.JSONArray;
import org.json.JSONObject;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


/**
 * Created by Administrator on 2018-03-07.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    TinyDB tinyDB;

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        sendRegistrationToServer(FirebaseInstanceId.getInstance().getToken());
    }


    private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.
        tinyDB = new TinyDB(this);
        String user_no = TextUtils.isEmpty(tinyDB.getString("user_no")) ? "0" : tinyDB.getString("user_no");
        tinyDB.putString("token", token);
        PushRegister pushRegister = new PushRegister();
        pushRegister.execute(user_no, token);
    }


    class PushRegister extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            OkHttpClient client = new OkHttpClient();
            FormBody.Builder formBodyBuilder = new FormBody.Builder();
            formBodyBuilder.add("no", strings[0]);
            formBodyBuilder.add("token", strings[1]);
            FormBody formBody = formBodyBuilder.build();
            try {
                Request request = new Request.Builder().url("http://13.125.241.78:3000/user/add_token").post(formBody).build();
                Response response = client.newCall(request).execute();
                String myResponse = response.body().string();
                return myResponse;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONArray resArray = new JSONArray(s);
                JSONObject resObj = resArray.getJSONObject(0);
                String user_no = resObj.getString("u_no");
                tinyDB.putString("user_no", user_no);

                Log.d("registered", tinyDB.getString("user_no") + " / " + tinyDB.getString("token"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
