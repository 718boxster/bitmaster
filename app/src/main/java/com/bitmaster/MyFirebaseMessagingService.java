package com.bitmaster;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;



/**
 * Created by Administrator on 2018-03-07.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    StaticData staticData = StaticData.getInstance();
    NotificationCompat.Builder notificationBuilder;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.d("asdfasdf", remoteMessage.getData().toString());
        sendPushNotification("CoinList", remoteMessage.getData().get("message"));
    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.fcm_push_icon: R.drawable.fcm_push_icon;
    }

    private void sendPushNotification(String title, String message){
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        Intent intent = new Intent();
        if(staticData.exchangeData.size() == 0) {
            intent = new Intent(this, SplashActivity.class);
        } else{
            intent = new Intent(this, MainActivity.class);
        }



        intent.putExtra("click", "refresh");

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_CANCEL_CURRENT);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            String channelID = "CustomerOderApp";
            String channelName = "getMsg";
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(channelID, channelName, importance);
            notificationManager.createNotificationChannel(mChannel);

            notificationBuilder = new NotificationCompat.Builder(getApplicationContext(), channelID)
                    .setSmallIcon(getNotificationIcon())
                    .setTicker(title)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setPriority(Notification.PRIORITY_MAX)
                    .setContentIntent(pendingIntent);
        } else {
            notificationBuilder = new NotificationCompat.Builder(getApplicationContext())
                    .setSmallIcon(getNotificationIcon())
                    .setTicker(title)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setPriority(Notification.PRIORITY_MAX)
                    .setContentIntent(pendingIntent);
        }

        PowerManager pm = (PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.ON_AFTER_RELEASE, "PushIdentificator");
        wakeLock.acquire(6000);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());

        wakeLock.release();
    }


}
